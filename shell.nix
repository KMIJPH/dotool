{ pkgs ? import <nixpkgs> { } }:
# nix-channel 23.11
let nixPackages = with pkgs; [ go ];
in pkgs.mkShell { nativeBuildInputs = nixPackages; }
