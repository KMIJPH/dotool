<!---
title: dotool
description: dotool readme
author: KMIJPH
created: 30 Oct 2022 14:24:41
last_modified: 22 May 2023 20:47:15
-->


# dotool

A simple dotfile utility tool to symlink dotfiles to their targets
or copy them to another directory (e.g. to make them public).

## Features

- [x] List dotfiles to symlink/copy in a json file
- [x] Specify patterns for files to ignore when copying them to another directory
- [x] Specify patterns for content to mask in the files themselves

## Usage

```
USAGE: dotool [options] <args>
  For more help visit: https://codeberg.org/KMIJPH/dotool
OPTIONS:
  -help	show this message
  -c	copies dotfiles from the source dir to the target dir
  -l	symlinks dotfiles from the source dir
  -s string
    	path to settings json file
  -v	show version
```

### Copy example

```json
{
    "target_dir": "~/gitrepo/dotfiles",
    "source_dir": "/path/to/dotfiles",
    "remove_dot": true,
    "targets": [
        "~/.config/nvim",
        "~/.bashrc"
    ],
    "mask_files": [
        ".*.pdf"
    ]
}
```

```
/path/to/dotfiles
 ├── .config/nvim
 │     ├── test.pdf
 │     └── init.lua
 └── .bashrc

~/
 ├── Documents
 ├── Downloads
 └── Pictures
```

```bash
dotool -c -s settings.json
```

```
~/gitrepo
 └── dotfiles
      ├── config/nvim
      │     └── init.lua
      └── bashrc
```

### Symlink example

```json
{
    "source_dir": "/path/to/dotfiles",
    "remove_dot": false,
    "targets": [
        "~/.config/nvim",
        "~/.bashrc"
    ]
}
```

```
/path/to/dotfiles
 ├── .config/nvim
 └── .bashrc

~/
 ├── Documents
 ├── Downloads
 └── Pictures
```

```bash
dotool -l -s settings.json
```

```
~/
 ├── .config/nvim
 ├── Documents
 ├── Downloads
 ├── Pictures
 └── .bashrc
```

## Settings

An example json file can be found [here](https://codeberg.org/KMIJPH/dotfiles/src/branch/main/dotool.json).

`target_dir`, `source_dir`, `mask_files` and `targets` should be understandable (see examples).

`dotool` determines the path to the source of the files based on the targets
given to it. If the files in the source directory are stored without a leading `.`,
`"remove_dot": true` can be passed (in order to actually find the files).

`mask_content` accepts a list of regex patterns to mask.
It uses [golang's regexp.ReplaceAllString](https://pkg.go.dev/regexp%23Regexp.ReplaceAllString).

Example:
```json
 "mask_content" = [
   {
       "expression": "(?m)[a-z0-9._%+\\-]+@[a-z0-9.\\-]+\\.[a-z]{2,5}",
       "format": "%s"
   }
]
```
will try to replace every occurrence of an email address in the files with `__masked__`.

```json
 "mask_content" = [
   {
       "expression": "(?m)(\\s*\"lat\"\\s*:\\s*\"*)[0-9\\.]+(\"*)",
       "format": "${1}0${2}"
   }
]
```
will try to replace `"lat": 71.123` with `"lat": 0`.

## Building

```bash
go build -C src
```
