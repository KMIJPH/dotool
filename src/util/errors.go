// File Name: errors.go
// Description: Error logging
// Author: KMIJPH
// Repository: https://codeberg.org/KMIJPH
// License: GPLv3+
// Creation Date: 24 Oct 2022 19:11:19
// Last Modified: 26 Oct 2022 19:55:55

package util

import (
	"log"
)

// handles error
func HandleError(err error, message string) {
	if err != nil {
		log.Fatalf("%sERROR: %s: %s%s", RED, message, err, RESET)
	}
}

// handles warning
func HandleWarning(err error, message string) {
	if err != nil {
		log.Printf("%sWARNING: %s: %s%s", YELLOW, message, err, RESET)
	}
}

// handles success
func HandleSuccess(err error, message string) {
	if err == nil {
		log.Printf("%sSUCCESS: %s%s", GREEN, message, RESET)
	}
}
