// File Name: constant.go
// Description: 'Constant' values
// Author: KMIJPH
// Repository: https://codeberg.org/KMIJPH
// License: GPLv3+
// Creation Date: Mon 08 Aug 2022 02:39:54 PM CEST
// Last Modified: 30 Oct 2022 14:23:04

package util

import (
	"fmt"
	"os"
)

var (
	APPNAME    = "dotool"
	AUTHOR     = "KMIJPH"
	MAILTO     = fmt.Sprintf("%s@%s.org", AUTHOR, APPNAME)
	REPOSITORY = fmt.Sprintf("https://codeberg.org/%s/%s", AUTHOR, APPNAME)
	VERSION    = "unknown"
	SEP        = string(os.PathSeparator)
)
