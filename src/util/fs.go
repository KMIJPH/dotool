// File Name: fs.go
// Description: fs functions
// Author: KMIJPH
// Repository: https://codeberg.org/KMIJPH
// License: GPLv3+
// Creation Date: 29 Oct 2022 18:59:17
// Last Modified: 18 Nov 2022 10:19:08

package util

import (
	"errors"
	"io/fs"
	"io/ioutil"
	"os"
	"path"
	"path/filepath"
	"strings"
)

// checks if file exists
func FileExist(outputPath string) bool {
	_, err := os.Stat(outputPath)
	if errors.Is(err, os.ErrNotExist) {
		return false
	}
	return true
}

// checks whether file is symlink
func IsSymlink(path string) (bool, error) {
	file, err := os.Lstat(path)
	if err != nil {
		return false, err
	}

	if file.Mode()&os.ModeSymlink == os.ModeSymlink {
		return true, nil
	}

	return false, nil
}

// checks whether symlink is broken
func IsBrokenSymlink(path string) (bool, error) {
	file, err := os.Readlink(path)
	if err != nil {
		return false, err
	}

	return !FileExist(file), nil
}

// checks whether file is directory or not
func IsDir(path string) (bool, error) {
	file, err := os.Stat(path)
	if err != nil {
		return false, err
	}

	if file.IsDir() {
		return true, nil
	}

	return false, nil
}

// symlinks path to destination
func Symlink(path, destination string) {
	err := os.Symlink(path, destination)
	HandleError(err, "[FS] Could not symlink file")
}

// reads file
func ReadFile(path string) string {
	body, err := ioutil.ReadFile(path)
	HandleError(err, "[FS] Could not read file")
	return string(body)
}

// writes file
func WriteFile(path, content string) {
	var file *os.File
	defer file.Close()

	file, err := os.Create(path)
	HandleError(err, "[FS] Could not create file")
	_, err = file.WriteString(content)
	HandleError(err, "[FS] Could not write file")
}

// expands path
func Expand(path string) string {
	if strings.HasPrefix(path, "~/") {
		dirname, err := os.UserHomeDir()
		HandleError(err, "[FS] Could not get user home dir")
		path = filepath.Join(dirname, path[2:])
	}

	return path
}

// creates directory (recursively) if it doesn't exist
func MkDir(parts ...string) (string, error) {
	dir := path.Join(parts...)

	_, err := os.Stat(dir)

	if err == nil || errors.Is(err, os.ErrNotExist) {
		os.MkdirAll(dir, 0777)
	} else {
		return "", err
	}

	return dir, nil
}

// splits folder path into its pieces
func SplitDir(path string) []string {
	return strings.Split(path, SEP)
}

// returns parent directory
func Parent(path string) string {
	parent := filepath.Dir(path)
	if parent == SEP {
		parent = ""
	}
	return parent
}

// returns relative path, based on source dir
func MakeRelative(path, sourceDir string) string {
	return strings.Replace(path, sourceDir, "", 1)
}

// lists folders recursively
func ListFolders(path, sourceDir string) []string {
	var folders []string

	err := filepath.WalkDir(
		path,
		func(s string, d fs.DirEntry, err error) error {
			if err != nil {
				return err
			}
			if d.IsDir() {
				folders = append(folders, MakeRelative(s, sourceDir))
			}
			return nil
		},
	)
	HandleError(err, "[FS] Could not list dir")
	return folders
}

// lists directory recusively
func ListDir(path string) []string {
	var files []string

	err := filepath.WalkDir(
		path,
		func(s string, d fs.DirEntry, err error) error {
			if err != nil {
				return err
			}
			if !d.IsDir() {
				files = append(files, s)
			}
			return nil
		},
	)
	HandleError(err, "[FS] Could not list dir")
	return files
}
