// File Name: colors.go
// Description: CLI colors
// Author: KMIJPH
// Repository: https://codeberg.org/KMIJPH
// License: GPLv3+
// Creation Date: 21 Oct 2022 12:17:01
// Last Modified: 27 Oct 2022 17:40:52

package util

import (
	"runtime"
)

var (
	RESET  = "\033[0m"
	RED    = "\033[31m"
	GREEN  = "\033[32m"
	YELLOW = "\033[33m"
	BLUE   = "\033[34m"
	PURPLE = "\033[35m"
	CYAN   = "\033[36m"
	GRAY   = "\033[37m"
	WHITE  = "\033[97m"
)

// windows doesn't handle these escape codes too well
func init() {
	if runtime.GOOS == "windows" {
		RESET = ""
		RED = ""
		GREEN = ""
		YELLOW = ""
		BLUE = ""
		PURPLE = ""
		CYAN = ""
		GRAY = ""
		WHITE = ""
	}
}
