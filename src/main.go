// File Name: main.go
// Description: Dotfile utility tool
// Author: KMIJPH
// Repository: https://codeberg.org/KMIJPH
// License: GPLv3+
// Creation Date: 29 Oct 2022 13:33:23
// Last Modified: 12 Aug 2023 09:32:27

package main

import (
	"errors"
	"flag"
	"fmt"
	"os"
	"path"
	"sort"

	"codeberg.org/KMIJPH/dotool/re"
	"codeberg.org/KMIJPH/dotool/settings"
	"codeberg.org/KMIJPH/dotool/util"
)

// Application options (flags)
type Options struct {
	copy     bool
	link     bool
	settings string
	version  bool
}

// parses flags/arguments to options struct
func parseArgs() Options {
	opts := Options{}

	flag.BoolVar(&opts.version, "v", false, "show version")
	flag.BoolVar(&opts.copy, "c", false, "copies dotfiles from the source dir to the target dir")
	flag.BoolVar(&opts.link, "l", false, "symlinks dotfiles from the source dir")
	flag.StringVar(&opts.settings, "s", "", "path to settings json file")

	flag.Usage = func() {
		fmt.Fprintf(
			os.Stderr,
			"USAGE: %s [options] <args>\n  For more help visit: %s\n"+
				"OPTIONS:\n  -help\tshow this message\n",
			util.APPNAME,
			util.REPOSITORY,
		)
		flag.PrintDefaults()
	}

	flag.Parse()

	if opts.version {
		fmt.Println(util.VERSION)
		os.Exit(0)
	}

	if opts.settings == "" {
		fmt.Println("No settings file passed")
		flag.Usage()
		os.Exit(1)
	}

	return opts
}

// skip masked files
func Skip(target string, settings *settings.Settings) bool {
	for _, mask := range settings.MaskFiles {
		if re.MatchesPath(target, mask) ||
			re.MatchesPath(path.Join(settings.TargetDir, target), mask) {
			return true
		}
	}

	return false
}

// returns all directories that were listed in the settings
func GetFiles(settings *settings.Settings) []string {
	var files []string

	for _, target := range settings.Targets {
		// skip files that match the source regex
		if !Skip(target, settings) {
			source := re.DetermineSource(target, settings.RemoveDot)
			source = fmt.Sprintf("%s%s%s", settings.SourceDir, util.SEP, source)

			isdir, err := util.IsDir(source)
			util.HandleError(err, "File probably doesn't exist")
			if isdir {
				folders := util.ListFolders(source, settings.SourceDir)
				files = append(files, folders...)
			} else {
				files = append(files, util.MakeRelative(source, settings.SourceDir))
			}
		}
	}

	var skipped []string
	// skip files that match the target regex
	for _, file := range files {
		if !Skip(file, settings) {
			skipped = append(skipped, file)
		}
	}

	// sort paths based on the smallest number of subdirectories they contain
	sort.Slice(skipped, func(i, j int) bool {
		return len(util.SplitDir(skipped[i])) < len(util.SplitDir(skipped[j]))
	})

	return skipped
}

// writes 'clean' files to target
func CleanFile(file string, settings *settings.Settings) {
	content := util.ReadFile(path.Join(settings.SourceDir, file))

	for _, mask := range settings.MaskContent {
		content = re.ReplaceCustom(content, mask.Expression, mask.Format)
	}
	util.WriteFile(path.Join(settings.TargetDir, file), content)
}

func main() {
	opts := parseArgs()
	settings := settings.ReadSettings(opts.settings)

	_, err := util.MkDir(settings.TargetDir)
	util.HandleError(err, "[FS] Could not create target directory")

	if opts.copy {
		files := GetFiles(&settings)

		// create folders
		for _, file := range files {
			isdir, err := util.IsDir(path.Join(settings.SourceDir, file))
			util.HandleError(err, "File probably doesn't exist")
			if isdir {
				_, err := util.MkDir(settings.TargetDir, file)
				util.HandleError(err, "[FS] Could not create directory")
			}
		}

		// write new files
		for _, file := range files {
			sourcePath := path.Join(settings.SourceDir, file)

			isdir, err := util.IsDir(sourcePath)
			util.HandleError(err, "File probably doesn't exist")
			if !isdir {
				CleanFile(file, &settings)
			} else {
				dirFiles := util.ListDir(sourcePath)

				for _, sub := range dirFiles {
					subPath := util.MakeRelative(sub, settings.SourceDir)

					// skip files that match the regex
					if Skip(subPath, &settings) {
						continue
					}
					CleanFile(subPath, &settings)
				}
			}
		}

	} else if opts.link {
		for _, target := range settings.Targets {
			target = util.Expand(target)
			source := re.DetermineSource(target, settings.RemoveDot)
			source = fmt.Sprintf("%s%s%s", settings.SourceDir, util.SEP, source)

			if !util.FileExist(source) {
				util.HandleError(errors.New(fmt.Sprintf("File '%s' does not exist", source)), "[FS]")
			}

			if util.FileExist(target) {
				err = os.RemoveAll(target)
				util.HandleError(err, "[LINK] Could not remove file")
				util.Symlink(source, target)
			} else {
				broken, err := util.IsBrokenSymlink(target)

				if err == nil {
					if broken {
						os.RemoveAll(target)
					}
				}
				util.Symlink(source, target)
			}
		}
	}
}
