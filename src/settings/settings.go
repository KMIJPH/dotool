// File Name: settings.go
// Description: Settings
// Author: KMIJPH
// Repository: https://codeberg.org/KMIJPH
// License: GPLv3+
// Creation Date: 29 Oct 2022 20:05:45
// Last Modified: 16 Nov 2022 13:01:43

package settings

import (
	"encoding/json"
	"errors"
	"os"

	"codeberg.org/KMIJPH/dotool/util"
)

// content mask expression and format
type ContentMask struct {
	Expression string `json:"expression"`
	Format     string `json:"format"`
}

// json to pass to the application
type Settings struct {
	TargetDir   string        `json:"target_dir"`
	SourceDir   string        `json:"source_dir"`
	RemoveDot   bool          `json:"remove_dot"`
	Targets     []string      `json:"targets"`
	MaskContent []ContentMask `json:"mask_content"`
	MaskFiles   []string      `json:"mask_files"`
}

// returns settings as json string
func (self *Settings) ToString() string {
	bytes, err := json.MarshalIndent(self, "", "    ")
	util.HandleError(err, "[JSON] Could not marshal settings")
	return string(bytes)
}

// reads settings from file
func ReadSettings(path string) Settings {
	content := util.ReadFile(path)

	var settings Settings
	err := json.Unmarshal([]byte(content), &settings)
	util.HandleError(err, "[JSON] Could not unmarshal settings")

	if len(settings.Targets) == 0 || settings.SourceDir == "" {
		util.HandleError(errors.New("No files passed"), "[SETTINGS]")
	}

	if settings.TargetDir == "" {
		settings.TargetDir, err = os.Getwd()
		util.HandleError(err, "[FS] Could not get current working directory")
	}
	settings.SourceDir = util.Expand(settings.SourceDir)
	settings.TargetDir = util.Expand(settings.TargetDir)

	return settings
}
