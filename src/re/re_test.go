// File Name: re_test.go
// Description: Regex tests
// Author: KMIJPH
// Repository: https://codeberg.org/KMIJPH
// License: GPLv3+
// Creation Date: 29 Oct 2022 13:42:12
// Last Modified: 30 Oct 2022 15:07:03

package re

import (
	"fmt"
	"os"
	"testing"

	"codeberg.org/KMIJPH/dotool/util"
)

func TestReplaceCustom(t *testing.T) {
	table := []struct {
		test     string
		exp      string
		format   string
		shouldBe string
	}{
		{
			"very simple example",
			"simple",
			"%s",
			fmt.Sprintf("very %s example", MASK),
		},
		{
			"more advanced example: this should be removed; not removed",
			"(.*: ).*(;.*)",
			"${1}%s${2}",
			fmt.Sprintf("more advanced example: %s; not removed", MASK),
		},
		{
			"simple number 01103",
			"([^0-9]*)[0-9]+",
			"${1}%s",
			fmt.Sprintf("simple number %s", MASK),
		},
		{
			`{"field1": 33, "field2": "value"},\n{"field3": 33}`,
			`(?m)(\s*"field2"\s*:\s*"*)\w+("*)`,
			"${1}key${2}",
			`{"field1": 33, "field2": "key"},\n{"field3": 33}`,
		},
		{
			`{"field1": 33.13021}`,
			`(?m)(\s*\"field1\"\s*:\s*\"*)[0-9\.]+("*)`,
			"${1}0${2}",
			`{"field1": 0}`,
		},
		{
			`somemail@provider.com`,
			`(?m)[a-z0-9._%+\-]+@[a-z0-9.\-]+\.[a-z]{2,4}`,
			"%s",
			MASK,
		},
		{
			`Host imap.provider.com\nPassCmd "gpg -d somfile.pgp | grep test"`,
			`(?m)(PassCmd )".*"\s*`,
			`${1}"%s"`,
			fmt.Sprintf(
				`Host imap.provider.com\nPassCmd "%s"`, MASK,
			),
		},
		{
			`Host imap.provider.com\nPassCmd "gpg -d somfile.pgp | grep test"`,
			`(?m)(Host )[\w\.]+\s*`,
			`${1}%s`,
			fmt.Sprintf(
				`Host %s\nPassCmd "gpg -d somfile.pgp | grep test"`, MASK,
			),
		},
	}

	for _, entry := range table {
		result := ReplaceCustom(entry.test, entry.exp, entry.format)
		if result != entry.shouldBe {
			t.Errorf(
				"ReplaceCustom(%s) was incorrect, got: %v, want: %v",
				entry.test, result, entry.shouldBe,
			)
		}
	}
}

func TestMatchesPath(t *testing.T) {
	table := []struct {
		test     string
		exp      string
		shouldBe bool
	}{
		{
			"/somepath/subpath/somefile.db",
			".*.db",
			true,
		},
		{
			"~/.Surge XT",
			`~/\.Surge XT`,
			true,
		},
		{
			"/anotherpath/subpath/file_i_dont_want.txt",
			".*./subpath/.*",
			true,
		},
		{
			"/anotherpath/another/subpath.txt",
			".*./subpath/.*",
			false,
		},
	}

	for _, entry := range table {
		result := MatchesPath(entry.test, entry.exp)
		if result != entry.shouldBe {
			t.Errorf(
				"MatchesPath(%s) was incorrect, got: %v, want: %v",
				entry.test, result, entry.shouldBe,
			)
		}
	}
}

func TestRemoveDot(t *testing.T) {
	table := []struct {
		test     string
		shouldBe string
	}{
		{
			`.config/somepath/file.txt`,
			"config/somepath/file.txt",
		},
		{
			`..config/somepath/file.txt`,
			".config/somepath/file.txt",
		},
	}

	for _, entry := range table {
		result := RemoveDot(entry.test)
		if result != entry.shouldBe {
			t.Errorf(
				"RemoveDot(%s) was incorrect, got: %v, want: %v",
				entry.test, result, entry.shouldBe,
			)
		}
	}
}

func TestDetermineSource(t *testing.T) {
	dirname, err := os.UserHomeDir()
	util.HandleError(err, "[FS] Could not get user home dir")

	table := []struct {
		test      string
		removeDot bool
		shouldBe  string
	}{
		{
			`~/.config/somepath/file`,
			false,
			".config/somepath/file",
		},
		{
			`~/.config/somepath/file`,
			true,
			"config/somepath/file",
		},
		{
			fmt.Sprintf(`%s/.config/somepath/file`, dirname),
			true,
			"config/somepath/file",
		},
		{
			"~/.bashrc",
			true,
			"bashrc",
		},
	}

	for _, entry := range table {
		result := DetermineSource(entry.test, entry.removeDot)
		if result != entry.shouldBe {
			t.Errorf(
				"DetermineSource(%s) was incorrect, got: %v, want: %v",
				entry.test, result, entry.shouldBe,
			)
		}
	}
}
