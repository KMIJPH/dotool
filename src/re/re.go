// File Name: re.go
// Description: Regex functions
// Author: KMIJPH
// Repository: https://codeberg.org/KMIJPH
// License: GPLv3+
// Creation Date: 29 Oct 2022 13:42:12
// Last Modified: 30 Oct 2022 14:23:25

package re

import (
	"fmt"
	"os"
	"regexp"
	"strings"

	"codeberg.org/KMIJPH/dotool/util"
)

const MASK = "__masked__"

// checks whether file matches an expression
func MatchesPath(path string, exp string) bool {
	folderRegex := regexp.MustCompile(exp)
	return folderRegex.MatchString(path)
}

// replaces expression
// example formats: '${1}%s${2}', '%s', '%s${1}'
func ReplaceCustom(content string, exp string, format string) string {
	format = strings.ReplaceAll(format, "%s", MASK)

	re := regexp.MustCompile(exp)
	return re.ReplaceAllString(content, format)
}

// removes dot from the beginning of a string
func RemoveDot(path string) string {
	re := regexp.MustCompile(`^\.(.*)`)
	path = re.ReplaceAllString(path, "${1}")
	return path
}

// determine relative path of the source based on the target
// source in this context means the file that should be symlinked to the target
func DetermineSource(target string, removeDot bool) string {
	var file string

	dirname, err := os.UserHomeDir()
	util.HandleError(err, "[FS] Could not get user home dir")
	s := regexp.QuoteMeta(util.SEP)
	dir := regexp.QuoteMeta(dirname)

	re := regexp.MustCompile(fmt.Sprintf(`^(%s|~)%s(.*)`, dir, s))
	file = re.ReplaceAllString(target, "${2}")

	if removeDot {
		file = RemoveDot(file)
	}

	return file
}
